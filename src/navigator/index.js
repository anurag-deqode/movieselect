import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListScreen from '../screens/ListScreen';
import DetailScreen from '../screens/DetailScreen';

const Stack = createStackNavigator();


const AppNavigation = () => {

    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName="ListScreen"
                screenOptions={{ gestureEnabled: false, headerShown: false }}>
                <Stack.Screen name="ListScreen" component={ListScreen} />
                <Stack.Screen name="DetailScreen" component={DetailScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigation;
