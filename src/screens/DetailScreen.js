import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';
import getMovieDetail from '../service/getMovieDetail';
import { IMAGE_BASE_URI } from '../config/appConfig';
import { Loader } from '../components/Loader';

export default DetailScreen = ({ route }) => {

    const { id } = route.params;
    const [data, setData] = useState({});
    const [isLoading, setisLoading] = useState(false);

    useEffect(() => setMovieDetails(), [setMovieDetails]);

    const setMovieDetails = async () => {
        setisLoading(true);
        const { response, error } = await getMovieDetail(id);
        if (response) setData(response);
        else setData({});
        setisLoading(false);
    }

    if (isLoading)
        return <Loader />;
    else {
        return (
            <View style={styles.container}>
                <View style={styles.imageBox}>
                    <Image source={{ uri: `${IMAGE_BASE_URI + data.poster_path}` }}
                        style={styles.image}
                    />
                </View>
                <View style={styles.contentBox}>
                    <View style={styles.titleBox}>
                        <Text style={styles.heading}>TITLE</Text>
                        <Text style={styles.data}>{data.title}</Text>
                    </View>
                    <View style={styles.ratingBox}>
                        <Text style={styles.heading}>Overview</Text>
                        <Text style={styles.data}>{data.overview}</Text>
                    </View>
                </View>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    container: {
        flex: appDimensions.one,
        backgroundColor: Colors.darkBlack,
        padding: appDimensions.ten,
        justifyContent: 'space-around'
    },
    imageBox: {
        flex: .5,
    },
    image: {
        height: '100%',
        width: '100%'
    },
    contentBox: {
        flex: .4,
    },
    titleBox: {
        flex: .2,
    },
    data: {
        color: Colors.grey,
        marginTop: appDimensions.five
    },
    heading: {
        color: Colors.grey,
        fontWeight: 'bold'
    },
    ratingBox: {
        flex: .8
    }
});
