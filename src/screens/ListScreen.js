import React, { useState, useEffect, useCallback } from 'react'
import { View, StyleSheet } from 'react-native'
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';
import SearchBar from '../components/SearchBar'
import MovieList from '../components/MovieList';
import getSearchedMovie from '../service/getSearchedMovie';
import { NoData } from '../components/NoData';
import { debounce } from '../service/debounce';

export default ListScreen = ({ navigation }) => {

    const [searchQuery, setSearchQuery] = useState('');
    const [data, setData] = useState([]);
    const [pageCurrent, setPageCurrent] = useState(1);

    useEffect(() => debounceWrapper(searchQuery), [searchQuery]);

    const debounceWrapper = useCallback(
        debounce((searchQuery) => onChangeText(searchQuery), 500),
        [],
    );

    const goToDetailScreen = (id) => {
        navigation.navigate('DetailScreen', {
            id
        })
    };

    const onChangeText = (text) => {
        setSearchQuery(text);
        setSearchedMovieData(text);
    };

    const setSearchedMovieData = async (text) => {
        const { response, error } = await getSearchedMovie(text, 1);
        if (response) setData(response.results);
        else setData([]);
    };

    const loadMoreData = async () => {
        setPageCurrent(pageCurrent + 1);
        const { response, error } = await getSearchedMovie(searchQuery, pageCurrent);
        if (response) setData(data.concat(response.results));
        else setData(data);

    }

    return (
        <View style={styles.container}>
            <SearchBar
                searchQuery={searchQuery}
                onChangeText={onChangeText}
            />
            {
                data?.length > 0 ?
                    <MovieList
                        data={data}
                        goToDetailScreen={goToDetailScreen}
                        loadMoreData={loadMoreData}
                    />
                    : <NoData />
            }
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.darkBlack,
        flex: appDimensions.one,
        marginLeft: appDimensions.five
    }
});
