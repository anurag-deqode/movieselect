import { API_BASE_URL, API_KEY } from "../config/appConfig";

const getMovieDetail = async (id) => {
    try {
        const response = await fetch(`${API_BASE_URL}/movie/${id}?api_key=${API_KEY}&language=en-US&page=1`);
        const responseJson = await response.json();
        return { response: responseJson }
    } catch (error) {
        return { error }
    }

}

export default getMovieDetail;
