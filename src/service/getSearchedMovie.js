import { API_BASE_URL, API_KEY } from "../config/appConfig";

const getSearchedMovie = async (query, page) => {
    try {
        const response = await fetch(`${API_BASE_URL}search/movie?api_key=${API_KEY}&language=en-US&query=${query}&page=${page}`);
        const responseJson = await response.json();
        return { response: responseJson }

    } catch (error) {
        return { error }
    }

}

export default getSearchedMovie;
