import React from 'react'
import { View, TextInput, StyleSheet, Text, TouchableOpacity } from 'react-native'
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';

const SearchBar = ({ searchQuery, onChangeText }) => {

    const clearText = () => onChangeText('')

    return (
        <View style={styles.searchBar}>
            <View>
                <TextInput
                    onChangeText={onChangeText}
                    value={searchQuery}
                    placeholder="Type Here..."
                    style={styles.textInput}

                />
            </View>
            <TouchableOpacity style={styles.circle} onPress={clearText}>
                <Text style={styles.closeIcon}>X</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    searchBar: {
        flexDirection: 'row',
        backgroundColor: Colors.darkBlack,
        margin: appDimensions.twelve,
        marginBottom: appDimensions.zero,
        justifyContent: 'space-between'
    },
    textInput: {
        fontSize: appDimensions.twentyFour,
        backgroundColor: Colors.darkBlack,
        width: '200%',
        borderWidth: appDimensions.one,
        borderColor: Colors.grey

    },
    circle: {
        height: appDimensions.fifty,
        width: appDimensions.fifty,
        borderRadius: appDimensions.hundered,
        borderWidth: appDimensions.one,
        borderColor: Colors.grey,
        alignSelf: 'center'
    },
    closeIcon: {
        color: Colors.grey,
        textAlign: 'center',
        fontSize: appDimensions.twenty,
        marginTop: appDimensions.six,
    }
});

export default SearchBar
