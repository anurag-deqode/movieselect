import React from 'react'
import { View, Text, FlatList, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';
import { IMAGE_BASE_URI } from '../config/appConfig';

const SCREEN_HEIGHT = Dimensions.get('screen').height;

const renderItem = (item, goToDetailScreen) => {

    const { vote_average, title, poster_path, id } = item;

    return (
        <TouchableOpacity style={styles.container} onPress={() => goToDetailScreen(id)}>
            <View style={styles.imageBox}>
                <Image source={{ uri: `${IMAGE_BASE_URI + poster_path}` }}
                    style={styles.image}
                />
            </View>
            <View style={styles.contentBox}>
                <View style={styles.titleBox}>
                    <Text style={styles.heading}>TITLE</Text>
                    <Text style={styles.data}>{title}</Text>
                </View>
                <View style={styles.ratingBox}>
                    <Text style={styles.heading}>RATING</Text>
                    <Text style={styles.data}>{vote_average}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default MovieList = ({ data, goToDetailScreen, loadMoreData }) => {

    return (
        <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => renderItem(item, goToDetailScreen)}
            onEndReached={() => loadMoreData()}
        />
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginHorizontal: appDimensions.fifteen,
        marginTop: appDimensions.forty,
        flex: appDimensions.one,
        height: SCREEN_HEIGHT / 7,
        justifyContent: 'space-around'
    },
    imageBox: {
        flex: .3,
        borderWidth: appDimensions.four,
        borderColor: Colors.grey
    },
    image: {
        height: '100%',
        width: '100%'
    },
    contentBox: {
        flex: .6,
    },
    titleBox: {
        flex: .6,
    },
    data: {
        color: Colors.grey,
        marginTop: appDimensions.five
    },
    heading: {
        color: Colors.grey,
        fontWeight: 'bold'
    },
    ratingBox: {
        flex: .4
    },
    loader: {
        marginTop: 10,
        alignItems: 'center'
    }
})