import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    horizontal: {
        padding: 10,
    },
    container: {
        flex: 1,
        backgroundColor: '#00000060',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 20,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});

const LoaderSize = 'large';

const Loader = () => (
    <View style={styles.container}>
        <ActivityIndicator size={LoaderSize} color="#ffffff" />
    </View>
);
export { Loader };
