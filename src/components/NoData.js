import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';

export const NoData = () => {

    return (
        <View style={styles.container}>
            <Text style={styles.text}>No Data</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: appDimensions.one,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: Colors.grey,
        fontSize: appDimensions.forty
    }
});