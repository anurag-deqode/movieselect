export const zero = 0;
export const one = 1;
export const two = 2;
export const four = 4;
export const five = 5;
export const six = 6;
export const ten = 10;
export const twelve = 12;
export const fifteen = 15;
export const twenty = 20;
export const twentyFour = 24;
export const forty = 40;
export const fifty = 50;
export const hundered = 100;

